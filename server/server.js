const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const User = require('./models/user');
const dotenv = require('dotenv');
 

dotenv.config();

const app = express()


mongodb.connect(
    process.env.DATABASE,
    { userNewUrlParse: true, useUnifiedTopology: True},
    (err) => {
    if (err){
        console.log(err);
    }else{
        console.log("Connected to the DB");
    }
});

// Middlewares

app.use(morgan("dev"));
app.use(bodyParser.json)
app.use(bodyParser.urlencoded( {extended: false} ));

app.post("/", (req, res) => {
    let user = new user();
    user.name = req.body.name;
    user.email = req.body.email;
    user.password = req.body.password;

    user.save((err) => {
        if(err){
            res.json(err);
        }else{
            res.json("successfully saved")
        }
    });
});


app.get("/", (req, res) => {
    res.json(" Hello Amazon Replica");
});

app.listen(34534, err => {
    if(err){
        console.log(err);
    }else {
        console.log("Listening on server ", 34534);
    }
        
}); 